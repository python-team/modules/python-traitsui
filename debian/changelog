python-traitsui (8.0.0-2) unstable; urgency=medium

  * Team upload.
  * remove extraneous dependency on python3-six (Closes: #1053967)

 -- Alexandre Detiste <tchet@debian.org>  Tue, 02 Jan 2024 02:19:21 +0100

python-traitsui (8.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #1052789
  * Standards-Version: 4.6.2 (routine-update)
  * Patches applied upstream
  * Build-Depends: pybuild-plugin-pyproject
  * Cleanup d/copyright

 -- Andreas Tille <tille@debian.org>  Fri, 27 Oct 2023 08:15:43 +0200

python-traitsui (7.2.1-2) unstable; urgency=medium

  * Team upload.
  * Drop python3-nose from B-D
  * d/rules: Remove PYBUILD_TEST_ARGS, no longer needed with unittest

 -- Nilesh Patra <nilesh@debian.org>  Mon, 22 Aug 2022 12:29:12 +0530

python-traitsui (7.2.1-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andreas Tille ]
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * watch file standard 4 (routine-update)
  * Versioned Build-Depends: python3-traits (>= 6), python3-pyface (>= 7)
  * Add missing Build-Depends and nocheck opstions
  * Some Python3.10 fixes and one ignored test
  * Fix Homepage

 -- Andreas Tille <tille@debian.org>  Wed, 09 Feb 2022 15:22:43 +0100

python-traitsui (6.1.3-3) unstable; urgency=medium

  * Team upload.
  * Fix SyntaxWarnings in rgb_color_trait.py files

 -- Scott Talbert <swt@techie.net>  Mon, 30 Dec 2019 00:10:29 -0500

python-traitsui (6.1.3-2) unstable; urgency=medium

  * Team upload.
    - Mostly no-change but needed to let the package migrate to testing.
  * Change pypi.python.org links to pypi.org.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 24 Dec 2019 14:00:17 +0300

python-traitsui (6.1.3-1) unstable; urgency=medium

  [ Scott Talbert ]
  * Team Upload.
  * d/watch: switch to https and remove orig-tar
  * Update to new upstream release 6.1.3
  * Remove d/orig-tar.sh
  * Switch to Python 3 and modernize packaging (Closes: #938224)
  * Remove wxpython3.0 patch - we are now using Qt backend
  * d/source/options: ignore changes in _version.py file
  * d/rules: Remove Windows Thumbs.db file
  * d/copyright: Convert to DEP5 format

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout

 -- Scott Talbert <swt@techie.net>  Tue, 10 Dec 2019 20:26:19 -0500

python-traitsui (4.5.1-1) unstable; urgency=medium

  * New upstream release
  * Acknowledge previous NMU changes, thanks
  * Bump Standards-Version to 3.9.6
  * Fix the watch file

 -- Varun Hiremath <varun@debian.org>  Sun, 16 Aug 2015 21:41:09 -0400

python-traitsui (4.4.0-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Improve wxpython3.0.patch so mayavi2 works.

 -- Olly Betts <olly@survex.com>  Sat, 20 Sep 2014 00:05:20 +0000

python-traitsui (4.4.0-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Update to use wxpython3.0 (Closes: #758695):
    - New patch: wxpython3.0.patch

 -- Olly Betts <olly@survex.com>  Sun, 31 Aug 2014 20:18:46 +0000

python-traitsui (4.4.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Set permissions in /usr/lib/python*, not /usr/share/pyshared
    Closes: #750253

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 06 Jul 2014 21:06:30 +0100

python-traitsui (4.4.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Varun Hiremath ]
  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Varun Hiremath <varun@debian.org>  Sat, 15 Mar 2014 23:52:35 -0400

python-traitsui (4.1.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3

 -- Varun Hiremath <varun@debian.org>  Mon, 23 Apr 2012 16:05:43 -0400

python-traitsui (4.0.1-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Sun, 10 Jul 2011 13:38:38 -0400

python-traitsui (4.0.0-1) unstable; urgency=low

  * New upstream release
  * Rename package to python-traitsui
  * Update package name in d/control and d/rules
  * Update debian/watch
  * Update debian/copyright
  * Bump Standards-Version to 3.9.2

 -- Varun Hiremath <varun@debian.org>  Sat, 09 Jul 2011 13:57:39 -0400

python-traitsgui (3.6.0-3) unstable; urgency=low

  * Team upload.
  * Rebuild to pick up 2.7 and drop 2.5 from supported versions
  * d/control: Bump Standards-Version to 3.9.2

 -- Sandro Tosi <morph@debian.org>  Sat, 07 May 2011 10:50:42 +0200

python-traitsgui (3.6.0-2) unstable; urgency=low

  * d/rules: fix pyshared directory path

 -- Varun Hiremath <varun@debian.org>  Wed, 06 Apr 2011 19:46:42 -0400

python-traitsgui (3.6.0-1) unstable; urgency=low

  * New upstream release
  * Convert to dh_python2 (Closes: #617040)

 -- Varun Hiremath <varun@debian.org>  Wed, 06 Apr 2011 00:18:30 -0400

python-traitsgui (3.5.0-1) experimental; urgency=low

  * New upstream release
  * d/control: Bump Standards-Version to 3.9.1
  * d/copyright: Fix BSD license references

 -- Varun Hiremath <varun@debian.org>  Sun, 17 Oct 2010 22:30:01 -0400

python-traitsgui (3.4.0-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 03 Jun 2010 01:52:31 -0400

python-traitsgui (3.3.0-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - removed Ondrej from Uploaders: thanks for your work!

  [ Varun Hiremath ]
  * New upstream release
  * Switch to source format 3.0
  * Bump Standards-Version to 3.8.4
  * Bump dh compat to 7
  * Remove transition package: python-enthought-traits-ui

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Feb 2010 13:50:54 -0500

python-traitsgui (3.1.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.2

 -- Varun Hiremath <varun@debian.org>  Mon, 20 Jul 2009 21:04:20 -0400

python-traitsgui (3.0.4-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 26 Mar 2009 19:11:46 -0400

python-traitsgui (3.0.3-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Varun Hiremath ]
  * New upstream release
  * debian/control: add python-setupdocs to Build-Depends

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Dec 2008 22:43:54 -0500

python-traitsgui (3.0.2-1) experimental; urgency=low

  * New upstream release
  * Rename the package to match upstream naming convention
  * debian/control: add python-traits, python-traitsbackendwx |
    python-traitsbackendqt to Depends
  * Update debian/watch and debian/orig-tar.sh files

 -- Varun Hiremath <varun@debian.org>  Sun, 26 Oct 2008 00:54:16 -0400

enthought-traits-ui (2.0.5-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.0
  * Remove patch range_editor.diff, merged upstream

 -- Varun Hiremath <varun@debian.org>  Fri, 11 Jul 2008 23:14:29 +0530

enthought-traits-ui (2.0.4-3) unstable; urgency=low

  * Add range_editor.diff patch from upstream (Closes: #478844)

 -- Varun Hiremath <varun@debian.org>  Tue, 13 May 2008 17:39:23 +0530

enthought-traits-ui (2.0.4-2) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - added versioned build-dep on python-central (>= 0.6)
  * debian/rules
    - fixed due to new python-central directories format (Closes: #472009)
    - removed unconditional rm on "usr/lib"

  [ Varun Hiremath ]
  * Make some minor fixes in debian/rules

 -- Varun Hiremath <varun@debian.org>  Sun, 23 Mar 2008 10:55:49 +0530

enthought-traits-ui (2.0.4-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Mon, 10 Mar 2008 13:02:59 +0530

enthought-traits-ui (2.0.3-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Sat, 01 Mar 2008 21:53:54 +0530

enthought-traits-ui (2.0.1b1-3) unstable; urgency=low

  * Add python-wxgtk2.6 to Depends

 -- Varun Hiremath <varun@debian.org>  Tue, 15 Jan 2008 13:38:39 +0530

enthought-traits-ui (2.0.1b1-2) unstable; urgency=low

  [ Varun Hiremath ]
  * Initial release (Closes: #459350)
  * Rename the source and binary packages
  * debian/rules:
    + add clean target
    + Use cdbs variable for upstream version in get-orig-source
    + install using setup files
  * debian/control:
    + Add myself to Uploaders
    + Depends: fix python-ets-traits version
    + Set architecture to all
  * Install docs and examples
  * Add debain/orig-tar.sh script to create orig.tar.gz
  * Update debian/copyright file
  * Update debian/watch file
  * Add debian/README.Debian-source file

  [ Sandro Tosi ]
  * debian/control
    - removed /svn/ from Vcs-Svn
    - added op=log to Vcs-Browser

 -- Varun Hiremath <varun@debian.org>  Sun, 06 Jan 2008 00:09:54 +0530

traits-ui (2.0.1b1-1) UNRELEASED; urgency=low

  * First unnofficial package revision

 -- Ondrej Certik <ondrej@certik.cz>  Fri, 07 Sep 2007 00:26:58 +0200
